﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parking.Model;
using Parking.Model.Vehicles;

namespace Parking.Writer
{
    public class EmptyWriter : IWriter
    {
        public void AddBalance(int carId, double money)
        {
            throw new NotImplementedException("Добавьте Writer перед его использованием");
        }

        public void ShowAutos(IEnumerable<Vehicle> vehicles)
        {
            throw new NotImplementedException("Добавьте Writer перед его использованием");
        }

        public void ShowTransactions(IEnumerable<Transaction> transactions)
        {
            throw new NotImplementedException("Добавьте Writer перед его использованием");
        }

        public void Write(string message, bool showLine = true)
        {
            throw new NotImplementedException("Добавьте Writer перед его использованием");
        }

        public void WriteException(string exeption)
        {
            throw new NotImplementedException("Добавьте Writer перед его использованием");
        }

        public void WriteInfo(string messag, bool showLine = true)
        {
            throw new NotImplementedException("Добавьте Writer перед его использованием");
        }

        public void WriteMenu(IEnumerable<string> menu)
        {
            throw new NotImplementedException("Добавьте Writer перед его использованием");
        }
    }
}
