﻿using Parking.Model;
using Parking.Model.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parking.Writer
{
    public interface IWriter
    {
        void ShowTransactions(IEnumerable<Transaction> transactions);
        void ShowAutos(IEnumerable<Vehicle> vehicles);
        void WriteException(string exeption);
        void Write(string message, bool showLine = true);
        void AddBalance(int carId, double money);
        void WriteInfo(string message, bool showLine = false);
        void WriteMenu(IEnumerable<string> menu);
    }
}
