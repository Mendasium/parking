﻿using Parking.Logger;
using Parking.Model;
using Parking.Model.Vehicles;
using Parking.Writer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Parking
{
    class Program
    {//private
        static void Main(string[] args)
        {
            ParkingInterface parkingInterface = new ParkingInterface();
            parkingInterface.Start();
        }
    }
}
