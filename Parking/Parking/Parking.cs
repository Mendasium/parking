﻿using Parking.Logger;
using Parking.Model;
using Parking.Model.Vehicles;
using Parking.Writer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parking
{
    public class Parking 
    {
        private Random rnd = new Random();
        private static readonly Parking parking = new Parking();
        private readonly List<Vehicle> registeredVehicles;
        private readonly List<Vehicle> removedVehicles;
        private readonly List<Transaction> lastTransations;
        private readonly List<StayedVehicle> stayedVehicles;
        private PayCycle payCycle;

        private bool payStarted = false;

        public double Balance { get; private set; }

        private ILogger<Transaction> logger;
        private IWriter writer;

        private Parking()
        {
            logger = new EmptyLogger<Transaction>();
            writer = new EmptyWriter();
            registeredVehicles = new List<Vehicle>();
            lastTransations = new List<Transaction>();
            stayedVehicles = new List<StayedVehicle>();
            removedVehicles = new List<Vehicle>();
            Balance = Settings.PrimaryBalance;
        }

        //Получение экземпляра
        public static Parking GetInstance() => parking;

        //Установка логера и интерфейса вывода
        public void SetLoger(ILogger<Transaction> logger)
        {
            this.logger = logger;
        }
        public void SetWriter(IWriter writer)
        {
            this.writer = writer;
        }
        public void SetPayCycle()
        {
            payCycle = new PayCycle(lastTransations, logger, stayedVehicles, registeredVehicles);
        }

        //Начало/конец работы парковки
        public void StartWork()
        {
            if (!payStarted)
            {
                payCycle.Start();
                payStarted = true;
            }
            else
            {
                writer.WriteException("Стоянка уже работает, вроде как");
            }  
        }
        public void StopWork()
        {
            if (payStarted)
            {
                payCycle.Stop();
                payStarted = false;
            }
            else
            {
                writer.WriteException("Стоянка уже не работает");
            }
        }

        //Очистка сохраненных данных
        public void ClearSavedData()
        {
            logger.ClearData();
        }

        //Добавление/удаление автомобиля с БД стоянки
        public void AddVehicle(Vehicle auto)
        {
            Vehicle car = GetAuto(auto.id);
            if (car != null)
            {
                writer.WriteException("Такой транспорт уже присутствует на стоянке");
                return;
            }

            auto.id = GenerateNewId();
            registeredVehicles.Add(auto);
            writer.Write(string.Format("Транспорт {0} успешно добавлен", auto));
        }
        public void RemoveVehicle(int autoId)
        {
            Vehicle car = GetAuto(autoId);
            if (car == null)
            {
                writer.WriteException("Такого транспорта на стоянке нету");
                return;
            }
            if(stayedVehicles.FirstOrDefault(x => x.vehicleId == autoId) != null)
            {
                writer.WriteException("Транспорт сначала нужно забрать со стоянки");
                return;
            }

            removedVehicles.Add(car);
            registeredVehicles.Remove(car);
            writer.Write(string.Format(@"Транспорт {0} успешно удален", car));
        }
            //Дополнительные функции
        private int GenerateNewId()
        {
            int index = rnd.Next(1000);
            while (registeredVehicles.Find(v => v.id == index) != null)
                index = rnd.Next(1000);
            return index;
        }
        private Vehicle GetAuto(int autoId)
        {
            return registeredVehicles.FirstOrDefault(x => x.id == autoId);
        }


        //Отображение списка автомобилей
        public void ShowCars()
        {
            if(registeredVehicles.Count == 0)
            {
                writer.WriteException("Транспорт отсутствует");
                return;
            }
            writer.ShowAutos(registeredVehicles);
        }
        public void ShowStayedCars()
        {
            if (stayedVehicles.Count == 0)
            {
                writer.WriteException("Транспорт отсутствует");
                return;
            }
            writer.ShowAutos(stayedVehicles.Select(v => registeredVehicles.FirstOrDefault(x => x.id == v.vehicleId)));
        }


        //Получение свободных/занятых мест на стоянке
        public int GetEmptyPlacesCount()
        {
            int stayedCount = stayedVehicles.Count;
            return Settings.VehicleMaxCount - stayedCount;
        }
        public int GetBusyPlacesCount()
        {
            return stayedVehicles.Count;
        }

        //Установка/удаление авто со стоянки
        public void SetToParking(int autoId)
        {
            Vehicle car = GetAuto(autoId);
            if (car == null)
            {
                writer.WriteException("Такого транспорта не существует");
                return;
            }
            if(stayedVehicles.Find(v => v.vehicleId == autoId) != null)
            {
                writer.WriteException("Этот транспорт уже стоит на парковке");
                return;
            }
            if (GetEmptyPlacesCount() <= 0)
            { 
                writer.WriteException("Все места на парковке заняты");
                return;
            }

            stayedVehicles.Add(new StayedVehicle
            {
                vehicleId = car.id,
                time = DateTime.Now
            });
            writer.Write(string.Format("Транспорт {0} поставлен на стоянку в {1}", car, DateTime.Now.TimeOfDay));
        }
        public void RemoveFromParking(int autoId)
        {
            Vehicle car = GetAuto(autoId);
            if (car == null)
            {
                writer.WriteException("Такого транспорта не существует");
                return;
            }
            StayedVehicle stayedCar = stayedVehicles.FirstOrDefault(v => v.vehicleId == autoId);
            if (stayedCar == null)
            {
                writer.WriteException("Такого транспорта на стоянке нету");
                return;
            }
            if(stayedCar.summ > car.Balance)
            {
                writer.WriteException(string.Format("Недостаточно средств у транспорта {0}, еще необходимо: {1}", 
                    stayedCar.vehicleId, Math.Abs(car.Balance - stayedCar.summ)));
                return;
            }

            Transaction transaction = new Transaction(stayedCar.vehicleId, stayedCar.summ, DateTime.Now);
            lastTransations.Add(transaction);
            car.RemoveBalance(stayedCar.summ);
            Balance += stayedCar.summ;
            stayedVehicles.Remove(stayedCar);
            writer.Write(string.Format("Транспорт {0} убран со стоянки в {1}, оставшаяся сумма {2}", car, DateTime.Now.TimeOfDay, car.Balance));
        }

        //Сумма заработанных денег за последнюю минуту
        public double LastMinuteEarnings()
        {
            return lastTransations.Select(t => t.Summ).Sum();
        }

        //Вывод транзакций за последнюю минуту
        public void ShowTransactionByMinute()
        {
            if (lastTransations.Count == 0)
            {
                writer.WriteException("Транзакции за последнюю минуту не производились");
                return;
            }
            writer.ShowTransactions(lastTransations);
        }

        //Вывод транзакций за все время
        public void ShowAllTransation()
        {
            var allTransactions = logger.LoadData();
            if(allTransactions == null)
            {
                writer.WriteException("Файл с данными был поврежден, он будет скопирован в папку PreviousLogs, программа продолжить записывать данные с нуля.");
                return;
            }
            if (allTransactions.Count() == 0 && lastTransations.Count == 0)
            {
                writer.WriteException("Транзакции отсутствуют");
                return;
            }
            writer.ShowTransactions(Enumerable.Concat(allTransactions,lastTransations));
        }

        //Пополнить баланс транспортного средства
        public void ReplenishBalance(int carId, double money)
        {
            Vehicle vehicle = GetAuto(carId);
            vehicle.AddBalance(money);
            writer.AddBalance(carId, money);
        }
    }
}

//+Узнать текущий баланс Парковки.
//+Сумму заработанных денег за последнюю минуту.
//+Узнать количество свободных/занятых мест на парковке.
//+Вывести на экран все Транзакции Парковки за последнюю минуту
//+Вывести всю историю Транзакций (считав данные из файла Transactions.log)
//+Вывести на экран список всех Транспортных средств
//+Создать/поставить Транспортное средство на Парковку
//+Удалить/забрать Транспортное средство с Парковки
//+Пополнить баланс конкретного Транспортного средства
