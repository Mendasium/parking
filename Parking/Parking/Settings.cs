﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parking
{
    /// <summary>
    /// Класс настроек, в котором хранятся глобальные переменные программы
    /// </summary>
    public static class Settings
    {
        public static int VehicleMaxCount { get; set; } = 10;
        public static double PrimaryBalance { get; set; } = 0;
        public static double SecondsToPay { get; set; } = 5;
        public static int SecondsToSave { get; set; } = 60;
        public static double Fine { get; set; } = 2.5;
        public static Dictionary<string, double> Rate = new Dictionary<string, double>()
        {
            { "Auto", 2 },
            { "Truck", 3.5 },
            { "Bus", 5 },
            { "Motorcycle", 1 }
        };
    }
}
