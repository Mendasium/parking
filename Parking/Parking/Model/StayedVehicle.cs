﻿using Parking.Model.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parking.Model
{
    public class StayedVehicle
    {
        public int vehicleId;
        public DateTime time;
        public double summ;
        public bool FineStarted { get; private set; } = false;
        
        public void StartFine()
        {
            FineStarted = true;
        }
    }
}
