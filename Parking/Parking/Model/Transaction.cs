﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Parking.Model
{
    /// <summary>
    /// Транзакция по оплате за услугу стоянки
    /// </summary>
    [DataContract]
    public class Transaction
    {
        [DataMember]
        public DateTime Time { get; private set; }
        [DataMember]
        public int VehicleId { get; private set; }
        [DataMember]
        public double Summ { get; private set; }

        public Transaction(int vehicleId, double summ, DateTime time)
        {
            Time = time;
            VehicleId = vehicleId;
            Summ = summ;
        }
    }
}
