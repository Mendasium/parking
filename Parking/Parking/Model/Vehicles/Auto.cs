﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parking.Model.Vehicles
{
    /// <summary>
    /// Родитель автомобилей, начальный класс, от которого образуются все типы авто
    /// </summary>
    public class Auto : Vehicle
    {
        public override double Rate => Settings.Rate["Auto"];
    }
}
