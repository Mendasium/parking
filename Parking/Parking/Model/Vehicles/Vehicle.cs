﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Parking.Model.Vehicles
{
    [KnownType(typeof(Auto))]
    [KnownType(typeof(Truck))]
    [KnownType(typeof(Bus))]
    [KnownType(typeof(Motorcycle))]
    public abstract class Vehicle
    {
        public int id;
        public double Balance { get; private set; }
        public abstract double Rate { get; }

        public override string ToString()
        {
            return id.ToString();
        }

        public void AddBalance(double money)
        {
            if(money > 0)
                Balance += money;
        }

        public void RemoveBalance(double money)
        {
            if (money < Balance)
                Balance -= money;
            else
                throw new Exception(string.Concat(@"У авто {0} недостаточно средств на счете({2}) для снятия {1}", id, money, Balance));
        }
        //public override bool Equals(object obj)
        //{
        //    if (!(obj is Vehicle))
        //        return false;
        //    Vehicle vehicle = obj as Vehicle;
        //    return vehicle.id == id && vehicle.Rate == Rate;
        //}

        //public override int GetHashCode()
        //{
        //    var hashCode = -1634;
        //    hashCode = hashCode * -1521 + id.GetHashCode();
        //    hashCode = hashCode * -1522 + Rate.GetHashCode();
        //    return hashCode;
        //}
    }
}
