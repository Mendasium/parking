﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parking.Model.Vehicles
{
    public class Motorcycle : Vehicle
    {
        public override double Rate => Settings.Rate["Motorcycle"];
    }
}
