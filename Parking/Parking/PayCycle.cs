﻿using Parking.Logger;
using Parking.Model;
using Parking.Model.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Parking
{
    public class PayCycle
    {
        private readonly int timeToSave = Settings.SecondsToSave;
        private readonly double timeToPay = Settings.SecondsToPay;
        private readonly double fine = Settings.Fine;
        Thread LogFiles;
        Thread ParkingPay;
        private bool stopped = false;

        public PayCycle(List<Transaction> transactions, ILogger<Transaction> logger,
            IEnumerable<StayedVehicle> stayedVehicles, IEnumerable<Vehicle> vehicles)
        {
            LogFiles = new Thread(delegate () { Save(transactions, logger); } );
            ParkingPay = new Thread(delegate () { PayForParking(stayedVehicles, vehicles); });
        } 

        void Save(List<Transaction> transactions, ILogger<Transaction> logger)
        {
            while (!stopped)
            {
                lock (transactions)
                {
                    logger.WriteData(transactions);
                    transactions.Clear();
                }
                Thread.Sleep(timeToSave * 1000);
            }
        }


        void PayForParking(IEnumerable<StayedVehicle> stayedVehicles, IEnumerable<Vehicle> vehicles)
        {
            while (!stopped)
            {
                foreach(StayedVehicle stayedVehicle in stayedVehicles)
                {
                    Vehicle vehicle = vehicles.FirstOrDefault(v => v.id == stayedVehicle.vehicleId);
                    if (vehicle == null)
                        throw new Exception("На стоянке находится незарегистрированный транспорт");
                    double secondsPaying = vehicle.Rate;

                    if(!stayedVehicle.FineStarted && stayedVehicle.summ + secondsPaying > vehicle.Balance)
                        stayedVehicle.StartFine();

                    if (stayedVehicle.FineStarted)
                        stayedVehicle.summ += secondsPaying * fine;
                    else
                        stayedVehicle.summ += secondsPaying;
                }
                Thread.Sleep((int)(timeToPay * 1000));
            }
        }

        public void Start()
        {
            LogFiles.Start();
            ParkingPay.Start();
        }

        public void Stop()
        {
            stopped = true;
        }
    }
}
