﻿using Parking.Logger;
using Parking.Model;
using Parking.Model.Vehicles;
using Parking.Writer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parking
{
    public class ParkingInterface
    {
        IWriter writer;
        private List<string> menu;
        private List<string> types;

        public ParkingInterface()
        {
            Parking.GetInstance().SetLoger(new JsonLogger<Transaction>());
            Parking.GetInstance().SetWriter(new ConsoleWriter());
            Parking.GetInstance().SetPayCycle();
            writer = new ConsoleWriter();

            menu = new List<string>();
            menu.Add(" 1 - Добавить транспорт в БД стоянки");
            menu.Add(" 2 - Удалить транспорт из БД стоянки");
            menu.Add(" 3 - Поставить транспорт на стоянку");
            menu.Add(" 4 - Снять транспорт со стоянки");
            menu.Add(" 5 - Пополнить баланс транспортного средства");
            menu.Add(" 6 - Вывести автомобили на стоянке");
            menu.Add(" 7 - Вывести все существующие автомобили");
            menu.Add(" 8 - Вывести транзакции с начала минуты");
            menu.Add(" 9 - Вывести все транзакции");
            menu.Add("10 - Вывести кол-во мест");
            menu.Add("11 - Узнать баланс стоянки");

            types = new List<string>();
            types.AddRange(Settings.Rate.Keys);
        }

        public void Start()
        {
            StartWork();
            int result = 0;
            while(true)
            {
                result = GetMenuItem();
                if (result == 0)
                    continue;
                if (result == 13)
                    break;
                Show(result);
            }
            FinishWork();
        }

        private void StartWork()
        {
            writer.WriteInfo("Вас приветсвует программа Парковки \"Моя остановочка\":)");
            writer.WriteInfo("Поскольку данная программа является тестовой, " +
                "то все данные с ее предыдущих запусков будут перемещены в папку PreviousLogs");
            Parking.GetInstance().ClearSavedData();
            writer.WriteInfo("Итак, приступим. \nДля перехода в меню вы всегда можете ввести \"МЕНЮ\".");
            writer.WriteInfo("Для выхода введите \"ВЫХОД\"",true);
            Parking.GetInstance().StartWork();
        }

        private void FinishWork()
        {
            Parking.GetInstance().StopWork();
            writer.Write("\t\t\t\tСпасибо что возпользовались программным продуктом:)");
            writer.WriteInfo("\t\t\t\t\t    Нажмите Enter для выхода");
            writer.Write("\t\t\t\t\t\tСчастливого дня:)");
            Console.ReadLine();
        }

        private string CurrentTime()
        {
            DateTime dateTime = DateTime.Now;
            return dateTime.Hour + ":" + string.Format("{0:d2}", dateTime.Minute) + ":" + string.Format("{0:d2}", dateTime.Second);
        }

        private int GetMenuItem()
        {
            writer.Write("\t\t\t\t\t\t\tМеню\t\t\t\t\t\t\t" + CurrentTime());
            writer.WriteMenu(menu);
            string result = "";
            int res = -1;
            while(res < 0)
            {
                result = Console.ReadLine();
                if (result == "МЕНЮ")
                    return 0;
                if (result == "ВЫХОД")
                    return 13;
                if (!int.TryParse(result, out res) || (res > 12 || res <= 0))
                {
                    res = -1;
                    writer.WriteInfo("Введите корректный номер пункта меню");
                }
            }
            return res;
        }

        private void AddAuto()
        {
            writer.Write("Введите тип транспорта", false);
            writer.WriteMenu(Enumerable.Range(1, types.Count).Select(i => i + " " + types[i - 1]));
            Vehicle vehicle = null;
            while (vehicle == null)
            {
                string result = Console.ReadLine();
                if (!int.TryParse(result, out int res) || (res > types.Count || res <= 0))
                {
                    writer.WriteException("Введите корректный тип");
                    continue;
                }
                vehicle = (Vehicle)Activator.CreateInstance(Type.GetType("Parking.Model.Vehicles." + types[res - 1]));
            }
            writer.Write("Введите баланс авто");
            while (vehicle.Balance == 0)
            {
                string result = Console.ReadLine();
                if (!double.TryParse(result, out double res) || res < 0)
                {
                    writer.WriteException("Введите корректный баланс");
                    continue;
                }
                vehicle.AddBalance(res);
            }
            Parking.GetInstance().AddVehicle(vehicle);
        }

        private void RemoveVehicle()
        {
            int carNumber = GetVehicleNumber();
            Parking.GetInstance().RemoveVehicle(carNumber);
        }

        private void ReplenishVehicleBalance()
        {
            int carNumber = GetVehicleNumber();
            writer.Write("Введите баланс авто для пополнения");
            double balance = 0;
            while (balance == 0)
            {
                string result = Console.ReadLine();
                if (!double.TryParse(result, out double res) || res < 0)
                {
                    writer.WriteException("Введите корректный баланс");
                    continue;
                }
                balance = res;
            }
            Parking.GetInstance().ReplenishBalance(carNumber, balance);
        }

        private void SetVehicleToParking()
        {
            int carNumber = GetVehicleNumber();
            Parking.GetInstance().SetToParking(carNumber);
        }

        private void RemoveVehicleFromParking()
        {
            int carNumber = GetVehicleNumber();
            Parking.GetInstance().RemoveFromParking(carNumber);
        }

        private int GetVehicleNumber()
        {
            writer.Write("Введите номер транспорта", false);
            int carNumber = -1;
            while (carNumber == -1)
            {
                string result = Console.ReadLine();
                if (!int.TryParse(result, out int res) || res <= 0)
                {
                    writer.WriteException("Введите номер");
                    continue;
                }
                carNumber = res;
            }
            return carNumber;
        }

        private void Show(int menuitem)
        {
            switch(menuitem)
            {
                case 1:
                    AddAuto();
                    break;
                case 2:
                    RemoveVehicle();
                    break;
                case 3:
                    SetVehicleToParking();
                    break;
                case 4:
                    RemoveVehicleFromParking();
                    break;
                case 5:
                    ReplenishVehicleBalance();
                    break;
                case 6:
                    writer.Write("Транспорт, находящийся на парковке:", false);
                    Parking.GetInstance().ShowStayedCars();
                    break;
                case 7:
                    writer.Write("Все автомобили в БД:", false);
                    Parking.GetInstance().ShowCars();
                    break;
                case 8:
                    writer.Write("Транзакции за последнюю минуту:", false);
                    Parking.GetInstance().ShowTransactionByMinute();
                    break;
                case 9:
                    writer.Write("Все осуществленные транзакции:", false);
                    Parking.GetInstance().ShowAllTransation();
                    break;
                case 10:
                    writer.Write("Кол-во мест на стоянке - " + Settings.VehicleMaxCount, false);
                    writer.Write("Кол-во свободных мест - " + Parking.GetInstance().GetEmptyPlacesCount(), false);
                    writer.Write("Кол-во занятых мест - " + Parking.GetInstance().GetBusyPlacesCount());
                    break;
                case 11:
                    writer.Write("Баланс стоянки составляет " + Parking.GetInstance().Balance + " условных единиц");
                    break;
                default:
                    writer.WriteException("Выберите корректный пункт меню");
                    break;
            }
        }
    }
}
