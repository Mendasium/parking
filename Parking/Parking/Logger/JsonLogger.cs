﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace Parking.Logger
{
    public class JsonLogger<T> : ILogger<T>
    {
        public static string path = Environment.CurrentDirectory;

        public void ClearData()
        {
            if (File.Exists(path + "/Transactions.log"))
            {
                SaveData();
            }
        }

        public IEnumerable<T> LoadData()
        {
            if (!File.Exists(path + "/Transactions.log"))
                return new List<T>();
            try
            {
                using (FileStream fs = new FileStream(path + "/Transactions.log", FileMode.OpenOrCreate))
                {
                    DataContractJsonSerializer reader = new DataContractJsonSerializer(typeof(IEnumerable<T>));
                    object list = reader.ReadObject(fs);
                    return (IEnumerable<T>)list;
                }
            }
            catch (SerializationException)
            {
                SaveData();
                return null;
            }
        }

        public void SaveData()
        {
            string data = File.ReadAllText(path + "/Transactions.log");
            DateTime nowTime = DateTime.Now;
            if(!Directory.Exists(path + "\\PreviousLogs"))
                Directory.CreateDirectory(path + "\\PreviousLogs");
            string localPath = path + "\\PreviousLogs\\" + nowTime.Day + "-" + nowTime.Hour + "-" + nowTime.Minute + "-" + nowTime.Second + ".log";
            File.WriteAllText(localPath, data);
            File.Delete(path + "/Transactions.log");
        }

        public void WriteData(IEnumerable<T> data)
        {
            IEnumerable<T> writtenData = data;
            if (File.Exists(path + "/Transactions.log"))
            {
                writtenData = LoadData();
                writtenData = writtenData.Concat(data);
            }
            using (FileStream fs = new FileStream(path + "/Transactions.log", FileMode.OpenOrCreate))
            {
                DataContractJsonSerializer writer = new DataContractJsonSerializer(typeof(IEnumerable<T>));
                writer.WriteObject(fs, writtenData);
            }
        }
    }
}
