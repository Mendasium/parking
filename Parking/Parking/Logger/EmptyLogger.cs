﻿using Parking.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parking.Logger
{
    public class EmptyLogger<T> : ILogger<T>
    {
        public void ClearData()
        {
            throw new NotImplementedException("Добавьте логгер перед его использованием");
        }

        public IEnumerable<T> LoadData()
        {
            throw new NotImplementedException("Добавьте логгер перед его использованием");
        }

        public void WriteData(IEnumerable<T> data)
        {
            throw new NotImplementedException("Добавьте логгер перед его использованием");
        }
    }
}
